SetCompressor /SOLID lzma

!include "MUI2.nsh"
!include "FileFunc.nsh"
!include LogicLib.nsh
!include "registerExtension.nsh"
!include "page_settings.nsh"

!define APP_NAME GTM-TestCreate
!define JAR_FILE nodesystem.jar
!define EXE_FILE testcreate.exe
!define LNK_FILE Testcreate-Studio.lnk

!define /file VERSION "version.info"

!define MUI_ICON icon.ico
!define MUI_UNICON uninstall.ico

Name ${APP_NAME}
# set the name of the installer
Outfile "gtm-testcreate-installer.exe"

Var startMenuFolder
Var idr
Var VERSION

# define the directory to install to, the desktop in this case as specified  
# by the predefined $DESKTOP variable
InstallDir $PROGRAMFILES\gtm

!define MUI_ABORTWARNING

#------------------------------------
#------------ Pages -----------------
#------------------------------------
!insertmacro MUI_PAGE_WELCOME
!define MUI_PAGE_CUSTOMFUNCTION_PRE fullInstallOnlyPage
!insertmacro MUI_PAGE_STARTMENU "Application" $startMenuFolder
!define MUI_PAGE_CUSTOMFUNCTION_PRE fullInstallOnlyPage
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

#Finish Page
!define MUI_FINISHPAGE_RUN "$INSTDIR\${EXE_FILE}"
!insertmacro MUI_PAGE_FINISH

#------------------------------------
#-------- Uninstall Pages -----------
#------------------------------------
!insertmacro MUI_UNPAGE_WELCOME
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_COMPONENTS
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"


DirText "Choose a folder in which to install GTM-TestCreate"

# create a default section.
Section
${If} $idr != ""
	ReadRegStr $INSTDIR HKLM "SOFTWARE\gtm-testcreate" "INSTDIR"
	Delete $INSTDIR\${JAR_FILE}
${EndIf}

SetOutPath $INSTDIR
CreateDirectory $INSTDIR\temp

# read the value from the registry into the $0 register
SetRegView 64
ReadRegStr $0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" CurrentVersion
SetRegView 32
${If} $0 == ""
ReadRegStr $0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" CurrentVersion
${EndIf}

${If} $0 != "1.8"
	NSISdl::download "http://javadl.oracle.com/webapps/download/AutoDL?BundleId=235696_2787e4a523244c269598db4e85c51e0c" "$INSTDIR\temp\JavaSetup.exe" 

	ExecWait '"$INSTDIR\temp\JavaSetup.exe"'

	ReadRegStr $0 HKLM "SOFTWARE\JavaSoft\Java Runtime Environment" CurrentVersion

	${If} $0 != "1.8"
		RMDir /r /REBOOTOK $INSTDIR\temp
		Abort "Failed to install Java 1.8!"
	${EndIf}

${EndIf}

# define what to install and place it in the output path
File ${EXE_FILE}
File ${JAR_FILE}
File version.info
File icon.ico

CreateDirectory $PROFILE\.gtm\gtm-testcreate

SetOutPath $INSTDIR\plugins
File /nonfatal /r plugins\*.*

SetOutPath $INSTDIR\modules
File /nonfatal /r modules\*.*

SetOutPath $INSTDIR\lib
File /nonfatal /r lib\*.*

${StrContains} $0 ">" "$startMenuFolder"
${If} $0 != ""
	StrCpy $startMenuFolder ""
${EndIf}

WriteUninstaller $INSTDIR\uninstall.exe

#Calculate Estimate size
${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
IntFmt $0 "0x%08X" $0

SetRegView 64
WriteRegStr HKLM "SOFTWARE\gtm-testcreate" "INSTDIR" "$INSTDIR"
WriteRegStr HKLM "SOFTWARE\gtm-testcreate" "STARTMENUFOLDER" "$startMenuFolder"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayName" "${APP_NAME}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "Publisher" "HTBLuVA Salzburg"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayVersion" "${VERSION}"
WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayIcon" "$INSTDIR\icon.ico"
WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
				 "EstimatedSize" "$0"
SetRegView 32
WriteRegStr HKLM "SOFTWARE\gtm-testcreate" "INSTDIR" "$INSTDIR"
WriteRegStr HKLM "SOFTWARE\gtm-testcreate" "STARTMENUFOLDER" "$startMenuFolder"


${If} $startMenuFolder == ""
	Goto End
${EndIf}

#Creating Start Menu
CreateDirectory "$SMPROGRAMS\$startMenuFolder"
createShortCut "$SMPROGRAMS\$startMenuFolder\${LNK_FILE}" "$INSTDIR\${EXE_FILE}"
createShortCut "$SMPROGRAMS\$startMenuFolder\Uninstall TestCreate.lnk" "$INSTDIR\uninstall.exe"

End:

${registerExtension} "Test Suite Project" ".tsp" "$INSTDIR\${EXE_FILE}"

RMDir /r /REBOOTOK $INSTDIR\temp

SectionEnd


Section "-Uninstall"

Delete $INSTDIR\uninstall.exe

Delete /REBOOTOK $INSTDIR\${EXE_FILE}
Delete /REBOOTOK $INSTDIR\${JAR_FILE}
Delete /REBOOTOK $INSTDIR\icon.ico
RMDir /REBOOTOK $INSTDIR


ReadRegStr $startMenuFolder HKLM "SOFTWARE\gtm-testcreate" "STARTMENUFOLDER"

SetRegView 64
DeleteRegValue HKLM "SOFTWARE\gtm-testcreate" "STARTMENUFOLDER"
DeleteRegValue HKLM "SOFTWARE\gtm-testcreate" "INSTDIR"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayName"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "UninstallString"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "Publisher"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayVersion"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayVersion"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
                 "DisplayIcon"
DeleteRegValue HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}" \
				 "EstimatedSize"
SetRegView 32
DeleteRegValue HKLM "SOFTWARE\gtm-testcreate" "STARTMENUFOLDER"
DeleteRegValue HKLM "SOFTWARE\gtm-testcreate" "INSTDIR"

${unregisterExtension} ".tsp" "$INSTDIR\${EXE_FILE}"

${If} $startMenuFolder == ""
	Goto End
${EndIf}

Delete "$SMPROGRAMS\$startMenuFolder\${LNK_FILE}"
Delete "$SMPROGRAMS\$startMenuFolder\Uninstall TestCreate.lnk"
RMDir /REBOOTOK "$SMPROGRAMS\$startMenuFolder"

End:

SectionEnd

Section /o "un.Delete Preferences" SecDelPrefs
	RMDir /r /REBOOTOK "$PROFILE\.gtm\gtm-testcreate"
SectionEnd

LangString DESC_SecDelPrefs ${LANG_ENGLISH} "Delete all Preferences from User folder."
#LangString DESC_SecDelPrefs ${LANG_GERMAN} "Löscht alle Einstellungen aus dem Benutzer Ordner."

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
	!insertmacro MUI_DESCRIPTION_TEXT ${SecDelPrefs} $(DESC_SecDelPrefs)
!insertmacro MUI_FUNCTION_DESCRIPTION_END

Function fullInstallOnlyPage
${If} $idr != ""
	Abort
${EndIf}
FunctionEnd

Function .onInit
	ReadRegStr $idr HKLM "SOFTWARE\gtm-testcreate" "INSTDIR"
FunctionEnd