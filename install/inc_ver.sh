#!/bin/bash
echo $REFRESH
if [ "$(echo $REFRESH)" = "false" ] 
then
	verOld=$(cat ./install/version.info)
	ver=(${verOld//./ })

	if [ $VERSION_ID -eq 0 ]
	then
		ver[1]=0
		ver[2]=0
	elif [ $VERSION_ID -eq 1 ]
	then
		ver[2]=0
	fi

	((ver[$VERSION_ID]++))

	echo "${ver[0]}.${ver[1]}.${ver[2]}" > install/version.info
	
	git push origin --delete latest
	git tag -f latest
	git push origin latest
	git tag -f $(cat install/version.info)
	git push origin $(cat install/version.info)
else
	git checkout latest
fi
