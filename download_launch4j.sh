#!/bin/bash
if [ ! -f $LAUNCH4J_ARCHIVE ]; then
    wget https://netcologne.dl.sourceforge.net/project/launch4j/launch4j-3/3.12/launch4j-3.12-linux-x64.tgz -O $CI_PROJECT_DIR/launch4j.tgz
fi