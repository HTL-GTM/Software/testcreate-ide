#!/bin/bash
git submodule update --init --recursive
cp -f splash.png nodesystem/src/main/resources/splash.png
cp -f logo.png nodesystem/src/main/resources/logo.png
cd nodesystem/
bash scripts/shared-scripts/install-deps.sh
if ! mvn $MAVEN_CLI_OPTS install; then
	exit 1
fi
git checkout -- src/main/resources/splash.png
git checkout -- src/main/resources/logo.png
#installed nodesystem, build modules
cd ..
cd testcreate-plugin
bash scripts/shared-scripts/install-deps.sh
if ! mvn $MAVEN_CLI_OPTS assembly:assembly; then
	exit 1
fi
cd ..
cd git_modules
for i in $(ls); do
cd "$i"
    [ -f scripts/shared-scripts/install-deps.sh ] && bash scripts/shared-scripts/install-deps.sh
	if ! mvn $MAVEN_CLI_OPTS clean package ; then
		exit 1
	fi
	cd ..
done
#All Modules built
cd ..
rm -rf install/modules
mkdir install/modules
find git_modules -iname "*.jar" -exec cp {} install/modules \;

if [ -d git_plugins ]
then
cd git_plugins/
for i in $(ls); do
	cd "$i"
    [ -f scripts/shared-scripts/install-deps.sh ] && bash scripts/shared-scripts/install-deps.sh
	if ! mvn $MAVEN_CLI_OPTS clean package ; then
		exit 1
	fi
	cd ..
done
cd ..

rm -rf install/plugins
mkdir install/plugins
find git_plugins/ -iname "*.jar" -exec cp {} install/plugins \;
fi #done plugins

rm install/nodesystem.jar
find testcreate-plugin/target/ -iname "*jar-with-dependencies.jar" -exec cp {} install/nodesystem.jar \;
